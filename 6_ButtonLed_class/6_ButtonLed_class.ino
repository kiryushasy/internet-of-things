boolean buttonWasUp = true;
boolean ledEnabled = false;
#define led 6
#define btn 2
#define check_time 20

void setup() {
  pinMode(6, OUTPUT);
  pinMode(2, INPUT_PULLUP);
}
 
void loop() {
   boolean buttonIsUp = digitalRead(btn);
 
   
   if (buttonWasUp && !buttonIsUp) {


      delay(check_time);

   
    buttonIsUp = digitalRead(btn);

      if (!buttonIsUp) {
         ledEnabled = !ledEnabled;
         digitalWrite(6, ledEnabled);
      }
   }
 
   buttonWasUp = buttonIsUp;
}