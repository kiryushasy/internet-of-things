# MQTT
Подключить две платы. Одна плата работает с датчиком света. Другая со светодиодом. Платы подключены к разным ноутбукам. Ноутбуки общаются по MQTT. Код подписчика и того кто публикует данные показаны ниже: <br>

Sunscriber:
```
import time
import paho.mqtt.client as mqtt_client
import random

def on_message(client, userdata, message):
    data = str(message.payload.decode("utf-8"))
    #topic = str(message.topic.decode("utf-8"))
    print(f"Received meassage on topic: {data}")

broker="broker.emqx.io"

client = mqtt_client.Client(f'lab_{random.randint(10000, 99999)}')
client.on_message = on_message

try:
    client.connect(broker)
except Exception:
    print('Failed to connect. Check network')
    exit()
    
client.loop_start()

wait_time = 5
sleep_time = 1    
while not client.is_connected():
    time.sleep(sleep_time)
    wait_time -= sleep_time
    if not wait_time:
        raise ValueError('Failed to connect. Timeout')
    
print('Subscribing')
client.subscribe('lab/room1/sensor')
time.sleep(600)
client.disconnect()
client.loop_stop()
print('Stop communication')
```
Publisher:
```
import time
import paho.mqtt.client as mqtt_client
import random

broker="broker.emqx.io"

client = mqtt_client.Client(f'lab_{random.randint(10000, 99999)}')

client.connect(broker)
client.publish('lab/room1/sensor', random.randint(100, 999))
client.disconnect()
```
1. (на стороне датчика) Реализовать передачу моментальных значений сеносора. Здесь и далее нормируйте показания сенсора, так чтобы они лежали в диапазоне от 0 до 100 <br>
`lab/UNIQUE_ID/photo/instant`
2. (на стороне датчика) Реализовать передачу усредненных данных по 100 значениям. Усреднение сделать бегущим средним. Для этого держите очередь из 100 значение на стороне ноутбука. <br>
`lab/UNIQUE_ID/photo/averge`
3. (на стороне датчика) Реализовать потоковую передачу данных: на микроконтроллер отправляется команда на постоянную посылку значений, ноутбук занимается пересылкой этих данных по MQTT. Время между двумя последовательными измерения должно быть таким чтобы программа на ноутбуке успевала обработать данные. Время подобрать автоматически: начать с большого времени (например 1с ) и увеличивать его до тех пор пока программа на ноутбуке будет справляться с потоком данных.<br>
`lab/UNIQUE_ID/photo/stream`
4. (на стороне светодида) Реализовать нахождение убывающих и возрастающих участков рядов измерений.  Учитывайте что измерения могут шуметь и поэтому алгоритм основанный на отдельных измерений будет не точным. Светодиод должен светиться только на участках убывающей яркости.
5. (на стороне светодида) Реализовать включение выключение по порогу освещенности. Порог брать как среднее между минимальным и максимальным значением освещенности. (на стороне датчика) Минимальные и максимальные значения обновлять при поступлении новых данных. <br>
`lab/UNIQUE_ID/photo/min`<br>
`lab/UNIQUE_ID/photo/max`