#include <FastLED.h>

const int DATA_PIN = 2;
const unsigned char NUM_LEDS = 15;

CRGB leds[NUM_LEDS];

void setup() {
  Serial.begin(9600);
  FastLED.addLeds<WS2812B, DATA_PIN>(leds, NUM_LEDS);
}

void set_colors() {
  unsigned char i = 0;
  while (i < NUM_LEDS && Serial.available() >= 3) {
    if (i == 0) delay(100);
    unsigned char r = Serial.read();
    unsigned char g = Serial.read();
    unsigned char b = Serial.read();
    leds[i++] = CRGB(r, g, b);
  }
}

void loop() {
  set_colors();
  FastLED.show();
}