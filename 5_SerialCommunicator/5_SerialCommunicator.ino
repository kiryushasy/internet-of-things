#define sensor_pin A0
#define led_pin 11

void setup() {
  pinMode(led_pin, OUTPUT);
  Serial.begin(9600);
}


void check_serial(){
  while (Serial.available() > 0) {
    char message = Serial.read();  
    if (message == 'u'){
      digitalWrite(led_pin, HIGH);
      //Serial.println("Led is ON");
    } else if (message == 'd') {
      digitalWrite(led_pin, LOW);
      //Serial.println("Led is OFF");
    } else if (message == 's') {
      char val = analogRead(sensor_pin) >> 2;
      Serial.write(val);
    }
  }
}

void loop() {
  check_serial();
}
